# Minecraft Paper Server

This Docker Image contains a Paper based Minecraft Server.

## Parent image

- debian:bookworm

## Dependencies

None

## Entrypoint & CMD

```
files/entrypoint.sh
```

## Build Image

### Automated Build

Through the new build pipeline all of our Docker images are built via Gitlab CI/CD and pushed to the Gitlab Container Registry. You usually don't need to build the image on your own, just push your commits to the git.

See all possible Container Tags in the Container Registry: https://gitlab.com/pazdzewicz_docker/minecraft-paper/container_registry

After the automated build is finished you can pull the image:

```
docker pull registry.gitlab.com/pazdzewicz_docker/minecraft-paper/MINECRAFT_VERSION:latest
```

All images also have a master tag, just replace the BRANCH with "master". The "latest" Tag is only an alias for the "master" tag.

### Manual Build

You can also build the Docker image on your local pc:

```
docker build -t "myminecraft-paper:latest" .
```

### Build Options

Build Options are Arguments in the Dockerfile (`--build-arg`):

- None

### Security Check during Build

Before we release Docker Containers into the wild it is required to run the Anchore security checks over the Pipeline (this doesn't apply to manual built images). You can download the current artifacts on the Pipelines page (the download drop-down).

## Environment Variables:


## Ports

- 25565

## Usage

### Generic Usage

