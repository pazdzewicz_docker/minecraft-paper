ARG DEBIAN_VERSION="bookworm"

FROM debian:${DEBIAN_VERSION}

ARG MINECRAFT_VERSION="1.20.1"
ARG OPENJDK_VERSION="17"

ENV MINECRAFT_VERSION ${MINECRAFT_VERSION}
ENV OPENJDK_VERSION ${OPENJDK_VERSION}

# Preperations
RUN apt-get update -y && \
    apt-get install -y \
                    curl \
                    jq \
                    openjdk-${OPENJDK_VERSION}-jre && \
    mkdir /opt/spigot

# Copy Server configs
COPY files /opt/spigot/
COPY config /opt/spigot/config

WORKDIR /opt/spigot/

RUN chmod +x /opt/spigot/entrypoint.bash && \
    chmod +x /opt/spigot/download-jar.bash && \
    /opt/spigot/download-jar.bash

# Start Server
ENTRYPOINT ["/opt/spigot/entrypoint.bash"]

EXPOSE 25565
