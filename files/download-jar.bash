#!/usr/bin/env bash
CURL="$(which curl)"
JQ="$(which jq)"
PAPERMC_API="https://api.papermc.io/v2/"

echo "DOWNLOAD MINECRAFT VERSION ${MINECRAFT_VERSION} FROM PAPERMC.IO"
echo "GET LATEST BUILD FOR ${MINECRAFT_VERSION}"
BUILD_VERSION="$(${CURL} -s -X 'GET' "${PAPERMC_API}/projects/paper/versions/${MINECRAFT_VERSION}" | ${JQ} .builds[-1])"
echo "GET BUILD INFOS FOR BUILD VERSION ${BUILD_VERSION}"
BUILD_INFOS="$(${CURL} -s -X 'GET' "${PAPERMC_API}/projects/paper/versions/${MINECRAFT_VERSION}/builds/${BUILD_VERSION}" | ${JQ} -r .downloads.application.name)"
echo "GET DOWNLOAD FOR ${BUILD_INFOS}"
${CURL} -X 'GET' "${PAPERMC_API}/projects/paper/versions/${MINECRAFT_VERSION}/builds/${BUILD_VERSION}/downloads/${BUILD_INFOS}" --output spigot.jar