#!/usr/bin/env bash
JAVA="$(which java)"
MEM_MIN="512M"
MEM_MAX="1024M"
OPT=""

$JAVA -Xms$MEM_MIN -Xmx$MEM_MAX $OPT -jar /opt/spigot/spigot.jar
